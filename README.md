This is Codility task for Barclays 2 task (3h 30min is given to complete tasks 1 and 2).

There is a game of war ships, the board is sized by a given number and the board is always square 
(example if the set number is 4 then the board is 4x4).
The task is to calculate the result and print out the number of hit ships and the number of sunken ships.
The hits and the ships are given in a string line.
Ships coordinates and size are described only by 2 coordinate, a single ship can take up to 4 coordinates,
its form can be a square or a line.
