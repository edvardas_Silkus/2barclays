/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Barclays_2task;

/**
 *
 * @author Edvar
 */
public class Barclays_2task {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //starting data
        int N = 12;
        String S = "1A 2A,12A 12A";
        String T = "12A 5A 7B 9D 13B 2A 19Q 17H";

        //creating table 
        boolean[][] table = new boolean[N][N];

//        For testing
//        for (int i = 0; i < N; i++) {
//            for (int j = 0; j < N; j++) {
//                System.out.print(" " + table[i][j]);
//            }
//            System.out.println(" ");
//        }
        //locating hits in the table
        if (T.isEmpty()) {
            T = " ";
        }

        String[] Tparts = T.split(" ");

        for (int i = 0; i < Tparts.length; i++) {
            String[] seperatedTParts = Tparts[i].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

            int Tpart1 = Integer.parseInt(seperatedTParts[0]);
            int Tpart2 = 27;
            switch (seperatedTParts[1].toLowerCase()) {
                case "a":
                    Tpart2 = 0;
                    break;
                case "b":
                    Tpart2 = 1;
                    break;
                case "c":
                    Tpart2 = 2;
                    break;
                case "d":
                    Tpart2 = 3;
                    break;
                case "e":
                    Tpart2 = 4;
                    break;
                case "f":
                    Tpart2 = 5;
                    break;
                case "g":
                    Tpart2 = 6;
                    break;
                case "h":
                    Tpart2 = 7;
                    break;
                case "i":
                    Tpart2 = 8;
                    break;
                case "j":
                    Tpart2 = 9;
                    break;
                case "k":
                    Tpart2 = 10;
                    break;
                case "l":
                    Tpart2 = 11;
                    break;
                case "m":
                    Tpart2 = 12;
                    break;
                case "n":
                    Tpart2 = 13;
                    break;
                case "o":
                    Tpart2 = 14;
                    break;
                case "p":
                    Tpart2 = 15;
                    break;
                case "q":
                    Tpart2 = 16;
                    break;
                case "r":
                    Tpart2 = 17;
                    break;
                case "s":
                    Tpart2 = 18;
                    break;
                case "t":
                    Tpart2 = 19;
                    break;
                case "u":
                    Tpart2 = 20;
                    break;
                case "v":
                    Tpart2 = 21;
                    break;
                case "w":
                    Tpart2 = 22;
                    break;
                case "x":
                    Tpart2 = 23;
                    break;
                case "y":
                    Tpart2 = 24;
                    break;
                case "z":
                    Tpart2 = 25;
                    break;

                default:
                    System.out.println("Error - String reading Hit Table");
                    break;
            }
//            For testing
//            System.out.println("part1: " + Tpart1);
//            System.out.println("part2: " + Tpart2);
            if (Tpart1 <= N && Tpart2 <= N) {
                table[Tpart1 - 1][Tpart2] = true;
            }

        }

//        For testing
//        for (int i = 0; i < N; i++) {
//            for (int j = 0; j < N; j++) {
//                System.out.print(" " + table[i][j]);
//            }
//            System.out.println(" ");
//        }
        //Generating ships on a table with hits
        if (S.isEmpty()) {
            S = " , ";
        }
        String[] ship = S.split(",");
        int sunk = 0;
        int hit = 0;
        int shipHits = 0;
        int shipVolume = 0;
        for (int k = 0; k < ship.length; k++) { //Number of ships
            shipHits = 0;
            shipVolume = 0;
            int[][] shipData = new int[2][2];
            String[] parts = ship[k].split(" ");

            for (int i = 0; i < parts.length; i++) {//amount of cordinates a ship have (1 or 2)
                String[] seperatedParts = parts[i].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

                int part1 = Integer.parseInt(seperatedParts[0]);
                int part2 = 27;
                switch (seperatedParts[1].toLowerCase()) {
                    case "a":
                        part2 = 0;
                        break;
                    case "b":
                        part2 = 1;
                        break;
                    case "c":
                        part2 = 2;
                        break;
                    case "d":
                        part2 = 3;
                        break;
                    case "e":
                        part2 = 4;
                        break;
                    case "f":
                        part2 = 5;
                        break;
                    case "g":
                        part2 = 6;
                        break;
                    case "h":
                        part2 = 7;
                        break;
                    case "i":
                        part2 = 8;
                        break;
                    case "j":
                        part2 = 9;
                        break;
                    case "k":
                        part2 = 10;
                        break;
                    case "l":
                        part2 = 11;
                        break;
                    case "m":
                        part2 = 12;
                        break;
                    case "n":
                        part2 = 13;
                        break;
                    case "o":
                        part2 = 14;
                        break;
                    case "p":
                        part2 = 15;
                        break;
                    case "q":
                        part2 = 16;
                        break;
                    case "r":
                        part2 = 17;
                        break;
                    case "s":
                        part2 = 18;
                        break;
                    case "t":
                        part2 = 19;
                        break;
                    case "u":
                        part2 = 20;
                        break;
                    case "v":
                        part2 = 21;
                        break;
                    case "w":
                        part2 = 22;
                        break;
                    case "x":
                        part2 = 23;
                        break;
                    case "y":
                        part2 = 24;
                        break;
                    case "z":
                        part2 = 25;
                        break;

                    default:
                        System.out.println("Error - String reading Ships");
                        break;
                }
                //small ship with only one cordinate
                if (parts.length == 1) {
                    if (table[part1 - 1][part2] == true) {
                        sunk++;
                    }
                }

                //big ship
                if (parts.length == 2) {
                    shipData[i][0] = part1 - 1;
                    shipData[i][1] = part2;
                }

            }//End of parts
            if (parts.length == 2) {

                //squire
                if (shipData[0][0] != shipData[1][0] && shipData[0][1] != shipData[1][1]) {
                    shipVolume = 4;
                    if (table[shipData[0][0]][shipData[0][1]] == true) {
                        shipHits++;
                    }
                    if (table[shipData[1][0]][shipData[1][1]] == true) {
                        shipHits++;
                    }
                    if (table[shipData[0][0]][shipData[1][1]] == true) {
                        shipHits++;
                    }
                    if (table[shipData[1][0]][shipData[0][1]] == true) {
                        shipHits++;
                    }
                    //vertical line (if numbers are the vertical line)   
                } else if (shipData[0][0] != shipData[1][0] && shipData[0][1] == shipData[1][1]) {
                    if (table[shipData[0][0]][shipData[0][1]] == true) {
                        shipHits++;
                    }
                    if (table[shipData[1][0]][shipData[1][1]] == true) {
                        shipHits++;
                    }

                    if (Math.abs(shipData[0][0] - shipData[1][0]) == 3) {
                        shipVolume = 4;

                        if (shipData[0][0] > shipData[1][0]) {
                            if (table[shipData[0][0] - 1][shipData[0][1]] == true) {
                                shipHits++;
                            }
                            if (table[shipData[0][0] - 2][shipData[0][1]] == true) {
                                shipHits++;
                            }
                        } else {
                            if (table[shipData[0][0] + 1][shipData[0][1]] == true) {
                                shipHits++;
                            }
                            if (table[shipData[0][0] + 2][shipData[0][1]] == true) {
                                shipHits++;
                            }
                        }

                    } else if (Math.abs(shipData[0][0] - shipData[1][0]) == 2) {
                        shipVolume = 3;

                        if (table[shipData[0][0] - (shipData[0][0] - shipData[1][0]) / 2][shipData[0][1]] == true) {
                            shipHits++;
                        }

                    } else {
                        shipVolume = 2;
                    }

                    //horrizontal line (if numbers are the vertical line)  
                } else if (shipData[0][0] == shipData[1][0] && shipData[0][1] != shipData[1][1]) {
                    if (table[shipData[0][0]][shipData[0][1]] == true) {
                        shipHits++;
                    }
                    if (table[shipData[1][0]][shipData[1][1]] == true) {
                        shipHits++;
                    }

                    if (Math.abs(shipData[0][1] - shipData[1][1]) == 3) {
                        shipVolume = 4;

                        if (shipData[0][1] > shipData[1][1]) {
                            if (table[shipData[0][0]][shipData[0][1] - 1] == true) {
                                shipHits++;
                            }
                            if (table[shipData[0][0]][shipData[0][1] - 2] == true) {
                                shipHits++;
                            }
                        } else {
                            if (table[shipData[0][0]][shipData[0][1] + 1] == true) {
                                shipHits++;
                            }
                            if (table[shipData[0][0]][shipData[0][1] + 2] == true) {
                                shipHits++;
                            }
                        }

                    } else if (Math.abs(shipData[0][1] - shipData[1][1]) == 2) {
                        shipVolume = 3;

                        if (table[shipData[0][0]][shipData[0][1] - (shipData[0][1] - shipData[1][1]) / 2] == true) {
                            shipHits++;
                        }

                    } else {
                        shipVolume = 2;
                    }
                }

                if (shipHits > 0) {
                    if (shipVolume == shipHits) {
                        sunk++;
                    } else {
                        hit++;
                    }
                }

            }
        }
        String returnValue = "";
        returnValue += Integer.toString(hit) + "," + Integer.toString(sunk);

        System.err.println(returnValue);

    }

}
